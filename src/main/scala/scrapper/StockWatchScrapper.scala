package dev.mateuszjaje
package scrapper

import model.InstrumentCode

import org.jsoup.Jsoup
import zio.Task

import java.net.URL

case class ScrappedData(code: InstrumentCode, stockPrice: BigDecimal)

object StockWatchScrapper {

  trait Service {
    def loadInfo(code: InstrumentCode): Task[ScrappedData]
  }

  object JsoupImpl extends Service {
    override def loadInfo(code: InstrumentCode) = {
      for {
        doc <- Task(Jsoup.parse(new URL(""), 10000))
      } yield ScrappedData(code, 0)
    }
  }
}
