package dev.mateuszjaje
package model

case class InstrumentCode(code: String) extends AnyVal

case class Instrument(code: InstrumentCode, humanName: String)

case class Dividend(code: InstrumentCode, amount: BigDecimal)
