import Syntax._

name := "stock-monitor"

version := "0.1"

scalaVersion := "2.13.4"

idePackagePrefix.invisible := Some("dev.mateuszjaje")

libraryDependencies ++= Seq(
  "org.jsoup"                      % "jsoup"                  % "1.+",
  "dev.zio"                       %% "zio"                    % "1.+",
  "com.softwaremill.sttp.client3" %% "core"                   % "3.0.0",
  "com.softwaremill.sttp.client3" %% "httpclient-backend-zio" % "3.0.0",
)
